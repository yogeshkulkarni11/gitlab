import { s__ } from '~/locale';

export const NO_RULE_MESSAGE = s__('SecurityOrchestration|No rules defined - policy will not run.');
