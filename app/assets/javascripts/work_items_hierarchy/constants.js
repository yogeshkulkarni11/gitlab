export const WORK_ITEMS_SURVEY_COOKIE_NAME = 'hide_work_items_hierarchy_survey';

/**
 * Hard-coded strings since we're rendering hierarchy
 * items from mock responses. Remove this when we
 * have a real hierarchy endpoint.
 */
export const LICENSE_PLAN = {
  FREE: 'free',
  PREMIUM: 'premium',
  ULTIMATE: 'ultimate',
};
